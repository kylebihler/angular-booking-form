export class Booking {
    constructor(
        public dateIn: Date,
        public length: number,
        public adults: number,
        public children: number,
        public rooms: number,
    ) {}
}