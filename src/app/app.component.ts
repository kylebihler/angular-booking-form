import { Component, OnInit } from '@angular/core';
import {Booking} from "./booking";
//103701

@Component({
    selector: 'app-img-booking',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {

    // selected = 'option2';
    public lengthOptions = [
        {"id": 1, "name": "1"},
        {"id": 2, "name": "2"},
        {"id": 3, "name": "3"},
        {"id": 4, "name": "4"},
        {"id": 5, "name": "5"},
        {"id": 6, "name": "6"},
        {"id": 7, "name": "7"},
        {"id": 8, "name": "8"},
        {"id": 9, "name": "9"},
        {"id": 10, "name": "10"},
        {"id": 11, "name": "11"},
        {"id": 12, "name": "12"},
    ];
    public adultOptions = [
        {"id": 1, "name": "1"},
        {"id": 2, "name": "2"},
        {"id": 3, "name": "3"},
        {"id": 4, "name": "4"},
    ];
    public childOptions = [
        {"id": 0, "name": "None"},
        {"id": 1, "name": "1"},
        {"id": 2, "name": "2"},
        {"id": 3, "name": "3"},
        {"id": 4, "name": "4"},
    ];
    public roomOptions = [
        {"id": 1, "name": "1"},
        {"id": 2, "name": "2"},
        {"id": 3, "name": "3"},
        {"id": 4, "name": "4"},
    ];

    model = new Booking( new Date(), 1, 1,  0, 1, );
    ngOnInit() {
    }
    onSubmit() {
        let tcUrl = 'https://reservations.travelclick.com/xxx'; //Add travelclick account number

        let dateString = '';

        let newDate = this.model.dateIn;

        dateString += (newDate.getMonth() + 1) + "/";
        dateString += newDate.getDate() + "/";
        dateString += newDate.getFullYear();

        tcUrl += "?DateIn=" + dateString + "&Adults=" + this.model.adults + "&Children=" + this.model.children + "&Length=" + this.model.length + "&Rooms=" + this.model.rooms+ "&languageid=1";
        console.log(tcUrl);
        window.open(tcUrl,'_self');
    }

}
